package livescripts.util;

import java.util.Collection;
import java.util.Iterator;

/**
 * 
 * @author areshko-aa, ?
 * 
 * @version 1.1.0, 17.07.2015, a.areshko
 *
 */
public enum StringUtil {
	;
	
	public static boolean IsEmpty(String value) {
		return (value == null) || value.equals("");
	}
	
	public static String implode(String[] array, String delimiter) {
		String result = "";
		
		for (int i = 0; i < array.length; i++) {
			result += array[i];
			if (i < array.length - 1) result += delimiter;
		}
		
		return result;
	}
	
}
