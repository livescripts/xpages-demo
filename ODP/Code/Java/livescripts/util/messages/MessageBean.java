package livescripts.util.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import com.ibm.xsp.component.UIViewRootEx2;
import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.ibm.xsp.util.JSUtil;

import livescripts.test.msgs.DGrowlMessages.DGrowlMsgType;
import livescripts.util.StringUtil;
import livescripts.util.messages.Message.MessageType;

public class MessageBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public void addMessage(final FacesMessage.Severity severity, final String summary, final String detail) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, summary, detail));
	}
	
	@SuppressWarnings("unchecked")
	public void postMessage(final Message.MessageType messageType, final String summary, final String detail) {		
		Message message = new Message(summary, detail);
		String channelName = messageType.getTypeName().toLowerCase();
		Map<Object, Object> flashScope = getFlashScope();
		//System.out.print("postMessage() - " + channelName);
		List<Object> messages = (List<Object>) flashScope.get(channelName + "Messages");
		if (messages == null) {
			messages = new ArrayList<Object>();
			flashScope.put(channelName + "Messages", messages);
		}
		messages.add(message);
		//System.out.print(channelName + ": " +((List)getFlashScope().get(channelName + "Messages")).size());
	}
	
	public void clearMessages(final MessageType messageType) {
		Map<Object, Object> flashScope = getFlashScope();
		String channelName = messageType.getTypeName().toLowerCase();
		flashScope.put(channelName + "Messages", null);		
	}
	
	public void clearMessages() {
		for (MessageType type : Message.messagesTypes) {
			clearMessages(type);
		}
	}
	
	/**
	 * 
	 * @param messageType
	 * @param summary
	 * @param duration if == 0 then - isSticky
	 */
	public void popupMessage(final Message.MessageType messageType, final String summary, final String detail, final Number duration) {
		StringBuilder result = new StringBuilder();
		result.append("dg.addNotification(");
		if (! StringUtil.IsEmpty(detail))
			JSUtil.addString(result, detail);
		result.append(",{'channel':");
		JSUtil.addString(result, messageType.getDGrowlChannel());
		if (! StringUtil.IsEmpty(summary)) {
			result.append(", 'title':");
			JSUtil.addString(result, summary);
		}
		if (duration.intValue() == 0)
			result.append(", 'sticky':true");
		else
			result.append(", 'duration':" + duration);
		result.append("});");
		
		getViewRoot().postScript(result.toString());
	}
	
	public static UIViewRootEx2 getViewRoot() {
		return (UIViewRootEx2)ExtLibUtil.resolveVariable(FacesContext.getCurrentInstance(), "view");
	}
	
	public boolean hasMessages() {
		return FacesContext.getCurrentInstance().getMessages().hasNext();
	}
	
	public String getStyleClass() {
		FacesMessage message = (FacesMessage) FacesContext.getCurrentInstance().getMessages().next();
		  Severity severity = message.getSeverity();
		  
		  String result = "alert fade in";
		  
		  if (severity == FacesMessage.SEVERITY_INFO)
		   result += " alert-info";
		  else if (severity == FacesMessage.SEVERITY_WARN)
		   result += " alert-warning";
		  else if (severity == FacesMessage.SEVERITY_ERROR || severity == FacesMessage.SEVERITY_FATAL)
		   result += " alert-danger";
		  
		  return result;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<FacesMessage> getMessages() {
		 Iterator<FacesMessage> it = FacesContext.getCurrentInstance().getMessages();
		  Collection<FacesMessage> result = new Vector<FacesMessage>();
		  
		  while (it.hasNext())
		   result.add(it.next());
		  return result;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<Object, Object> getFlashScope() {
		return (Map<Object, Object>)resolveVariable("flashScope");
	}
	
	public static Object resolveVariable(final String varName) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().getVariableResolver().resolveVariable(context, varName);
	}

}
