package livescripts.util.messages;

import java.io.Serializable;

import javax.faces.application.FacesMessage;

public class Message implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final String TYPE_INFO_NAME = "INFO";
	public static final MessageType TYPE_INFO = new MessageType(TYPE_INFO_NAME);
	
	private static final String TYPE_WARNING_NAME = "WARNING";
	public static final MessageType TYPE_WARNING = new MessageType(TYPE_WARNING_NAME);
	
	private static final String TYPE_SUCCESS_NAME = "SUCCESS";
	public static final MessageType TYPE_SUCCESS = new MessageType(TYPE_SUCCESS_NAME);
	
	private static final String TYPE_ERROR_NAME = "ERROR";
	public static final MessageType TYPE_ERROR = new MessageType(TYPE_ERROR_NAME);
	
	private static final String TYPE_FATAL_NAME = "FATAL";
	public static final MessageType TYPE_FATAL = new MessageType(TYPE_FATAL_NAME);
	
	public static final MessageType[] messagesTypes = {TYPE_FATAL, TYPE_ERROR, TYPE_WARNING, TYPE_SUCCESS, TYPE_INFO};
	
	private String summary;
	private String detail;
	
	public Message(final String summary, final String detail) {
		this.summary = summary;
		this.detail = detail;
	}
	
	public String getSummary() {
		return summary;
	}

	public String getDetail() {
		return detail;
	}
	
	public static class MessageType {
		
		String typeName;
		
		protected MessageType(final String typeName) {
			this.typeName = typeName;
		}
		
		public String getTypeName() {
			return typeName;
		}
		
		public FacesMessage.Severity getSeverity() {
			if (typeName == TYPE_INFO_NAME || typeName == TYPE_SUCCESS_NAME)
				return FacesMessage.SEVERITY_INFO;
			else if (typeName == TYPE_WARNING_NAME)
				return FacesMessage.SEVERITY_WARN;
			else if (typeName == TYPE_ERROR_NAME)
				return FacesMessage.SEVERITY_ERROR;
			else return FacesMessage.SEVERITY_FATAL;
		}
		
		public String getDGrowlChannel() {
			if (typeName == TYPE_INFO_NAME)
				return "info";
			else if (typeName == TYPE_SUCCESS_NAME)
				return "confirmation";
			else if (typeName == TYPE_WARNING_NAME)
				return "warning";
			else if (typeName == TYPE_ERROR_NAME || typeName == TYPE_FATAL_NAME)
				return "error";
			return "";
		}
		
	}

}
