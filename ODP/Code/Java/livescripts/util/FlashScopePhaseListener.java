package livescripts.util;

import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;

public class FlashScopePhaseListener implements PhaseListener {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public void afterPhase(PhaseEvent event) {
		System.out.print(getClass());
		FacesContext facesContext = event.getFacesContext();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
		if(!request.getMethod().equals("POST")) {
			Map flashScope = (Map)facesContext.getApplication().createValueBinding("#{flashScope}").getValue(facesContext);
			flashScope.clear();
		}
	}
	public void beforePhase(PhaseEvent event) { }
	
	public PhaseId getPhaseId() { return PhaseId.RENDER_RESPONSE; }

}
