package livescripts.test;

import java.io.Serializable;

import livescripts.test.msgs.DGrowlMessages;
import livescripts.test.msgs.DGrowlMessages.DGrowlMsgType;

public class MessageBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public void addMessage() {
		System.out.print(getClass());
		DGrowlMessages.addMessage(DGrowlMsgType.INFO, "Test", "Test message", false);
		DGrowlMessages.addMessage(DGrowlMsgType.ERROR, "Test", "Test message", true);
		DGrowlMessages.addMessage(DGrowlMsgType.ERROR, "Test", "Test message", false);
	}

}
