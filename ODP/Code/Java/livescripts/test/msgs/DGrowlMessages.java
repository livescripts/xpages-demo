package livescripts.test.msgs;

import javax.faces.context.FacesContext;

import livescripts.util.StringUtil;

import com.ibm.xsp.component.UIViewRootEx2;
import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.ibm.xsp.util.JSUtil;

public enum DGrowlMessages {
	
	;
	
	public enum DGrowlMsgType {
		
		INFO("info"), ERROR("error"), WARNING("warning"), CONFIRM("confirmation");
		
		private String styleClass;
		
		private DGrowlMsgType(String styleClass) {
			this.styleClass = styleClass;
		}
		
		String getStyleClass() {
			return styleClass;
		}
		
	}
	
	public static UIViewRootEx2 getViewRoot() {
		return (UIViewRootEx2)ExtLibUtil.resolveVariable(FacesContext.getCurrentInstance(), "view");
	}
	
	public static void addMessage(final DGrowlMsgType msgType, final String title, final String text, final boolean sticky) {
		StringBuilder result = new StringBuilder();
		result.append("dg.addNotification(");
		if (! StringUtil.IsEmpty(text))
			JSUtil.addString(result, text);
		result.append(",{'channel':");
		JSUtil.addString(result, msgType.getStyleClass());
		if (! StringUtil.IsEmpty(title)) {
			result.append(", 'title':");
			JSUtil.addString(result, title);
		}
		if (sticky)
			result.append(", 'sticky':true");
		else
			result.append(", 'duration':7000");
		result.append("});");
		
		getViewRoot().postScript(result.toString());
	}
	
}
