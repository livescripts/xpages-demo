function Msg_GetMessageType(type) {
	var msgType:livescripts.util.messages.Message.MessageType;
	
	if (type == "Info")
		msgType = livescripts.util.messages.Message.TYPE_INFO;
	else if (type == "Success")
		msgType = livescripts.util.messages.Message.TYPE_SUCCESS;
	else if (type == "Warning")
		msgType = livescripts.util.messages.Message.TYPE_WARNING;
	else if (type == "Error")
		msgType = livescripts.util.messages.Message.TYPE_ERROR;
	else if (type == "Fatal")
		msgType = livescripts.util.messages.Message.TYPE_FATAL;
	
	return msgType;
}

function Msg_GetSeverity(type) {
	var sev:javax.faces.application.FacesMessage.Severity;
	
	if (type == "Info" || type == "Success")
		sev = javax.faces.application.FacesMessage.SEVERITY_INFO;
	else if (type == "Warning")
		sev = javax.faces.application.FacesMessage.SEVERITY_WARN;
	else if (type = "Error")
		sev = javax.faces.application.FacesMessage.SEVERITY_ERROR;
	else if (type = "Fatal")
		sev = javax.faces.application.FacesMessage.SEVERITY_FATAL;
	
	return sev;
}

function Msg_PostMessage() {
	var type = getComponent("msgType").getValue();
	var title = getComponent("msgTitle").getValue();
	var text = getComponent("msgText").getValue();
	
	MessageBean.postMessage(Msg_GetMessageType(type), title, text);
}

function Msg_AddMessage() {
	var type = getComponent("msgType").getValue();
	var title = getComponent("msgTitle").getValue();
	var text = getComponent("msgText").getValue();
	
	MessageBean.addMessage(Msg_GetSeverity(type), title, text);
}

function Msg_PostMultiMessages() {
	var text = getComponent("msgText").getValue();
	
	MessageBean.postMessage(Msg_GetMessageType("Info"), "Info!", text);
	MessageBean.postMessage(Msg_GetMessageType("Success"), "Success!", text);
	MessageBean.postMessage(Msg_GetMessageType("Warning"), "Warning!", text);
	MessageBean.postMessage(Msg_GetMessageType("Error"), "Error!", text);
	MessageBean.postMessage(Msg_GetMessageType("Fatal"), "Fatal!", text);
}

function Msg_AddMultiMessages() {
	var text = getComponent("msgText").getValue();
	
	MessageBean.addMessage(Msg_GetSeverity("Info"), "Info!", text);
	MessageBean.addMessage(Msg_GetSeverity("Success"), "Success!", text);
	MessageBean.addMessage(Msg_GetSeverity("Warning"), "Warning!", text);
	MessageBean.addMessage(Msg_GetSeverity("Error"), "Error!", text);
	MessageBean.addMessage(Msg_GetSeverity("Fatal"), "Fatal!", text);
}